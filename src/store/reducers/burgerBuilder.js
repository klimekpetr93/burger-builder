import * as actionTypes from '../actions/actionTypes';

import { updateObject } from '../../shared/utility';

const initialState = {
  ingredients: null,
  totalPrice: 4,
  error: null,
  building: false,
};

const INGREDIENT_PRICES = {
  salad: 0.5,
  cheese: 0.3,
  meat: 1.4,
  bacon: 0.6,
};

//function for add or remove ingredient
const updateIngredient = (state, action, remove) => {
  return updateObject(state, {
    ingredients: updateObject(state.ingredients, {
      [action.ingredientName]: remove ? state.ingredients[action.ingredientName] - 1 : state.ingredients[action.ingredientName] + 1,
    }),
    totalPrice: remove ? state.totalPrice - INGREDIENT_PRICES[action.ingredientName] : state.totalPrice + INGREDIENT_PRICES[action.ingredientName],
    building: true,
  });
};

//set initial ingredient, or set ingredients in burger before checkout
const setIngredients = (state, action) => {
  return updateObject(state, {
    error: false,
    ingredients: {
      salad: action.ingredients.salad,
      bacon: action.ingredients.bacon,
      cheese: action.ingredients.cheese,
      meat: action.ingredients.meat,
    },
    totalPrice: 4,
    building: false,
  });
};

const fetchIngredientsFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
  });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_INGREDIENT:
      return updateIngredient(state, action, false);

    case actionTypes.REMOVE_INGREDIENT:
      return updateIngredient(state, action, true);

    case actionTypes.SET_INGREDIENTS:
      return setIngredients(state, action);

    case actionTypes.FETCH_INGREDIENTS_FAILED:
      return fetchIngredientsFailed(state, action);

    default:
      return state;
  }
};

export default reducer;
