import * as actionTypes from '../actions/actionTypes';

import { updateObject } from '../../shared/utility';

const initialState = {
  idToken: null,
  userID: null,
  error: null,
  loading: false,
  authorizationRedirect: '/',
};

const authorizationStart = (state, action) => {
  return updateObject(state, { error: null, loading: true });
};

const authorizationtSuccess = (state, action) => {
  return updateObject(state, {
    idToken: action.token,
    userID: action.userID,
    error: null,
    loading: false,
  });
};

const authorizationFail = (state, action) => {
  return updateObject(state, { error: action.error, loading: false });
};

const authorizationLogOut = (state, action) => {
  return updateObject(state, { idToken: null, userID: null });
};

const authorizationSetRedirect = (state, action) => {
  return updateObject(state, { authorizationRedirect: action.path });
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.AUTHORIZATION_START:
      return authorizationStart(state, action);
    case actionTypes.AUTHORIZATION_SUCCESS:
      return authorizationtSuccess(state, action);
    case actionTypes.AUTHORIZATION_FAIL:
      return authorizationFail(state, action);
    case actionTypes.AUTHORIZATION_LOGOUT:
      return authorizationLogOut(state, action);
    case actionTypes.AUTHORIZATION_SET_REDIRECT:
      return authorizationSetRedirect(state, action);
    default:
      return state;
  }
};

export default reducer;
