import * as actionTypes from './actionTypes';
import axios from 'axios';

export const authorizationStart = () => {
  return {
    type: actionTypes.AUTHORIZATION_START,
  };
};

export const authorizationSuccess = (idToken, localId) => {
  return {
    type: actionTypes.AUTHORIZATION_SUCCESS,
    token: idToken,
    userID: localId,
  };
};

export const authorizationFail = (error) => {
  return {
    type: actionTypes.AUTHORIZATION_FAIL,
    error: error,
  };
};

export const logOut = () => {
  localStorage.removeItem('token');
  localStorage.removeItem('epirationDate');
  localStorage.removeItem('userId');
  return {
    type: actionTypes.AUTHORIZATION_LOGOUT,
  };
};

export const checkAuthorizationTimeout = (exporitaionTime) => {
  return (dispatch) => {
    setTimeout(() => {
      dispatch(logOut());
    }, exporitaionTime * 100); //milisecunds needed transfer to secunds ...
  };
};

export const authorization = (email, password, isSignUp) => {
  return (dispatch) => {
    //tady budu zacinat authorizaci
    dispatch(authorizationStart());

    let url = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDNo72-qLfkBcWLbQ5nuStD-5xv7mOGcf8';
    if (!isSignUp) {
      url = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDNo72-qLfkBcWLbQ5nuStD-5xv7mOGcf8';
    }
    axios
      .post(url, {
        email: email,
        password: password,
        returnSecureToken: true,
      })
      .then((response) => {
        localStorage.setItem('token', response.data.idToken);
        localStorage.setItem('epirationDate', new Date(new Date().getTime() + response.data.expiresIn * 1000));
        localStorage.setItem('userId', response.data.localId);
        dispatch(authorizationSuccess(response.data.idToken, response.data.localId));
        dispatch(checkAuthorizationTimeout(response.data.expiresIn));
      })
      .catch((error) => {
        dispatch(authorizationFail(error.response.data.error));
      });
  };
};

export const authorizationSetRedirect = (path) => {
  return {
    type: actionTypes.AUTHORIZATION_SET_REDIRECT,
    path: path,
  };
};


export const authorizationCheckState = () => {
  return dispatch => {
    let token = localStorage.getItem('token');
    if (!token) {
      dispatch(logOut());
    } else {
      let epirationDate = new Date(localStorage.getItem('epirationDate'));
      if (epirationDate < new Date()) {
        dispatch(logOut())
      } else {
        dispatch(authorizationSuccess(token, localStorage.getItem('userId')));
        dispatch(checkAuthorizationTimeout((epirationDate.getTime() - new Date().getTime()) / 100));
      }
    }
  };
};



