import React, { useEffect, Suspense } from 'react';
import Layout from './containers/Layout/Layout';
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';
import { Route, withRouter, Redirect, Switch } from 'react-router';
import { connect } from 'react-redux';
import * as actions from './store/actions';

const Checkout = React.lazy(() => {
    return import('./containers/CheckOut/CheckOut');
});

const Authorization = React.lazy(() => {
    return import('./containers/Authorization/Authorization');
});

const Orders = React.lazy(() => {
    return import('./containers/Orders/Orders');
});

const LogOut = React.lazy(() => {
    return import('./containers/Authorization/LogOut/LogOut');
});

const App = props => {
    useEffect(() => {
        props.onTryAuthoSignIn();
    }, []);


    let routesHTML = (
        <Switch>
            <Route path="/login" render={(props) => <Authorization {...props} />} />
            <Route path="/" exact component={BurgerBuilder} />
            <Redirect to="/" />
        </Switch>
    );

    if (props.isAuthenticated) {
        routesHTML = (
            <Switch>
                <Route path="/checkout" render={(props) => <Checkout {...props} />} />
                <Route path="/orders" render={(props) => <Orders {...props} />} />
                <Route path="/logout"component={LogOut} />
                <Route path="/login" render={(props) => <Authorization {...props} />} />
                <Route path="/" exact component={BurgerBuilder} />
                <Redirect to="/" />
            </Switch>
        );
    }

    return (
        <div>
            <Layout><Suspense fallback={<p>...loading</p>}>{routesHTML}</Suspense></Layout>
        </div>
    );
}


const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.authorization.idToken !== null,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onTryAuthoSignIn: () => dispatch(actions.authorizationCheckState()),
    };
};
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
