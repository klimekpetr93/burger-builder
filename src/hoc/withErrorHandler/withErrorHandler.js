import React, { Component, useState, useEffect } from 'react';
import Modal from '../../components/UI/Modal/Modal';

const withErrorhandler = (WrappedComponent, axios) => {
    return (props) => {
        const [error, setError] = useState(null);

        const reqIntercetors = axios.interceptors.request.use(
            (req) => {
                setError(null);
                return req;
            }
        );

        const resIntercetors = axios.interceptors.response.use(
            (res) => res,
            (err) => {
                setError(err);
                return Promise.reject(err)
            });

        useEffect(() => {
            return () => {
                axios.interceptors.request.eject(reqIntercetors);
                axios.interceptors.response.eject(resIntercetors);
            }
        }, [reqIntercetors, resIntercetors]);

        const clearErrorHandler = () => {
            setError(null)
        };

        return (
            <>
                <Modal show={error} modalClosed={clearErrorHandler}>
                    {error && error.message}
                </Modal>
                <WrappedComponent {...props} />
            </>
        );
    };
};

export default withErrorhandler;
