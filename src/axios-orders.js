import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://burger-react-tut.firebaseio.com/',
});

export default instance;
