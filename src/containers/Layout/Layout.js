import React, { useState } from 'react';
import classes from './Layout.module.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';
import { connect } from 'react-redux';

const Layout = props => {
    const [sideDrawerShow, setSideDraweShow] = useState(false);

    const sideDrawerClosedHandler = () => {
        setSideDraweShow(false)
    };

    const sideDrawerOpenHandler = () => {
        setSideDraweShow(!sideDrawerShow);
    };

    return (
        <>
            <Toolbar isAuth={props.isAuthenticated} openSideDrawer={sideDrawerOpenHandler} />
            <SideDrawer isAuth={props.isAuthenticated} open={sideDrawerShow} closed={sideDrawerClosedHandler} />
            <main className={classes.Content}>{props.children}</main>
        </>
    );
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.authorization.idToken !== null,
    };
};

export default connect(mapStateToProps)(Layout);
