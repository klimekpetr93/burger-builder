import React, { Component, useState, useEffect } from 'react';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import axios from '../../axios-orders';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandle from '../../hoc/withErrorHandler/withErrorHandler';
import { connect } from 'react-redux';

import * as actions from '../../store/actions'; //index muzu vynechat podle konvenci mi ho vezme

const BurgerBuilder = (props) => {
    /// vice moderni styl
    const [purchasing, setPurchasing] = useState(false);

    useEffect(() => {
        props.onInitIngredients();
    }, []);

    const updatePurchaseState = (ingredients) => {
        const sum = Object.keys(ingredients)
            .map((igKey) => {
                return ingredients[igKey];
            })
            .reduce((sum, el) => {
                return sum + el;
            }, 0);

        return sum > 0;
    };

    const purchaseHandler = () => {
        if (props.isAuthenticated) {
            setPurchasing(true);
        } else {
            props.onSetRedirectPath('/checkout');
            props.history.push({ pathname: '/login' });
        }
    };

    const purchaseCancelHandler = () => {
        setPurchasing(false);
    };

    const purchaseContinueHandler = () => {
        props.onInitPurchase();
        props.history.push({ pathname: '/checkout' });
    };

    const disableInfo = {
        ...props.ingredients,
    };

    for (let key in disableInfo) {
        disableInfo[key] = disableInfo[key] <= 0;
    }

    let orderSummaryHTML = null;

    let burgerHTML = props.error ? <p>{props.error.message} </p> : <Spinner />;

    if (props.ingredients) {
        burgerHTML = (
            <>
                <Burger ingredients={props.ingredients} />
                <BuildControls
                    ingredientAdded={props.onIngredientAdded}
                    ingredientDelete={props.onIngredientRemoved}
                    disabled={disableInfo}
                    price={props.totalPrice}
                    purchasable={updatePurchaseState(props.ingredients)}
                    ordered={purchaseHandler}
                    isAuthenticated={props.isAuthenticated}
                />
            </>
        );

        orderSummaryHTML = (
            <OrderSummary
                ingredients={props.ingredients}
                purchaseContinued={purchaseContinueHandler}
                purchaseCancelled={purchaseCancelHandler}
                price={props.totalPrice}
            />
        );
    }

   /* if (state.loading) {
        orderSummaryHTML = <Spinner />;
    }*/

    return (
        <>
            <Modal show={purchasing} modalClosed={purchaseCancelHandler}>
                {orderSummaryHTML}
            </Modal>
            {burgerHTML}
        </>
    );
};

const mapStateToProps = (state) => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        error: state.burgerBuilder.error,
        isAuthenticated: state.authorization.idToken !== null,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onIngredientAdded: (nameIngredient) => dispatch(actions.addIngredient(nameIngredient)),
        onIngredientRemoved: (nameIngredient) => dispatch(actions.removeIngredient(nameIngredient)),
        onInitIngredients: () => dispatch(actions.initIngredients()),
        onInitPurchase: () => dispatch(actions.purchaseInit()),
        onSetRedirectPath: (path) => dispatch(actions.authorizationSetRedirect(path)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandle(BurgerBuilder, axios));
