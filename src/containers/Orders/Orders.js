import React, { Component, useEffect } from 'react';

import Order from '../../components/Order/Order';
import axios from '../../axios-orders';
import withErrorhandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../store/actions';
import { connect } from 'react-redux';
import Spinner from '../../components/UI/Spinner/Spinner';

const Orders = (props) => {
    //component did mount
    useEffect(() => {
        props.onFetchOrders(props.token, props.userID);
    }, []);

    let ordersHTML = <Spinner />;

    if (!props.loading) ordersHTML = props.orders.map((order) => <Order key={order.id} price={order.totalPrice} ingredients={order.ingredients} />);

    return <div>{ordersHTML}</div>;
};

const mapStateToProps = (state) => {
    return {
        orders: state.order.orders,
        loading: state.order.loading,
        token: state.authorization.idToken,
        userID: state.authorization.userID,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchOrders: (token, userID) => dispatch(actions.fetchOrders(token, userID)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorhandler(Orders, axios));
