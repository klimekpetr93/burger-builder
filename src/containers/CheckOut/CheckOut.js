import React from 'react';
import CheckOutSummary from '../../components/Order/CheckOutSummary/CheckOutSummary';
import { Route, Redirect } from 'react-router';
import ContactData from './ContactData/ContactData';
import { connect } from 'react-redux';

const CheckOut = (props) => {
    const checkoutCanceledHandler = () => {
        props.history.goBack();
    };

    const checkoutContinuedHandler = () => {
        props.history.replace('/checkout/contact-data');
    };

    let summary = <Redirect to={'/'} />;

    if (props.ingredients) {
        const purchasedRedirect = props.purchased ? <Redirect to={'/'} /> : null;
        summary = (
            <div>
                {purchasedRedirect}
                <CheckOutSummary
                    ingredients={props.ingredients}
                    onCheckoutContinued={checkoutContinuedHandler}
                    onCheckoutCanceled={checkoutCanceledHandler}
                />
                <Route path={props.match.path + '/contact-data'} component={ContactData} />
            </div>
        );
    }
    return summary;
};
const mapStateToProps = (state) => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        purchased: state.order.purchased,
    };
};

export default connect(mapStateToProps)(CheckOut);
