import React, { Component, useState } from 'react';
import Button from '../../../components/UI/Button/Button';
import classes from './ContactData.module.css';

import axios from '../../../axios-orders';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Input from '../../../components/UI/Input/Input';
import { connect } from 'react-redux';
import withErrorhandler from '../../../hoc/withErrorHandler/withErrorHandler';
import * as orderActions from '../../../store/actions';
import { updateObject, checkValidity } from '../../../shared/utility';

const ContactData = (props) => {
    const [formIsValid, setFormIsValid] = useState(false);
    const [orderForm, setOrderForm] = useState({
        name: {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'Your name',
            },
            value: '',
            validation: {
                required: true,
            },
            valid: false,
            touched: false,
        },
        street: {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'Your street',
            },
            value: '',
            validation: {
                required: true,
            },
            valid: false,
            touched: false,
        },
        postCode: {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'Your zipcode',
            },
            value: '',
            validation: {
                required: true,
                minLength: 5,
                maxLength: 10,
            },
            valid: false,
            touched: false,
        },
        city: {
            elementType: 'input',
            elementConfig: {
                type: 'text',
                placeholder: 'Your mesto',
            },
            value: '',
            validation: {
                required: true,
            },
            valid: false,
            touched: false,
        },
        email: {
            elementType: 'input',
            elementConfig: {
                type: 'email',
                placeholder: 'Your email',
            },
            value: '',
            validation: {
                required: true,
            },
            valid: false,
            touched: false,
        },
        deliveryMethod: {
            elementType: 'select',
            elementConfig: {
                options: [
                    {
                        value: 'fastest',
                        displayValues: 'Fastest',
                    },
                    {
                        value: 'cheapest',
                        displayValues: 'Cheapest',
                    },
                ],
            },
            validation: {},
            value: 'fastest',
            valid: true,
        },
    });

    const orderHandler = (event) => {
        event.preventDefault();
        let formData = {};
        for (let formElement in orderForm) {
            formData[formElement] = orderForm[formElement].value;
        }

        let order = {
            ingredients: props.ingredients,
            totalPrice: props.totalPrice,
            orderData: formData,
            userID: props.userID,
        };

        props.onOrderBurger(order, props.token);
    };

    const inputChangedHandler = (event, inputIdentifier) => {
        let updatedFormElement = updateObject(orderForm[inputIdentifier], {
            value: event.target.value,
            valid: checkValidity(event.target.value, orderForm[inputIdentifier].validation),
            touched: true,
        });

        let updatedOrderForm = updateObject(orderForm, {
            [inputIdentifier]: updatedFormElement,
        });

        let formIsValid = true;

        for (let input in updatedOrderForm) {
            formIsValid = updatedOrderForm[input].valid && formIsValid;
        }
        setOrderForm(updatedOrderForm);
        setFormIsValid(formIsValid);
    };

    let formElementArray = [];
    for (let key in orderForm) {
        formElementArray.push({
            id: key,
            config: orderForm[key],
        });
    }

    let formHTML = (
        <form onSubmit={orderHandler}>
            {formElementArray.map((formElement) => (
                <Input
                    key={formElement.id}
                    elementType={formElement.config.elementType}
                    elementConfig={formElement.config.elementConfig}
                    value={formElement.config.value}
                    invalid={!formElement.config.valid}
                    shouldValidate={formElement.config.validation}
                    touched={formElement.config.touched}
                    changed={(event) => inputChangedHandler(event, formElement.id)}
                />
            ))}
            <Button btnType="Success" disabled={!formIsValid}>
                ORDER
            </Button>
        </form>
    );

    if (props.loading) formHTML = <Spinner />;

    return (
        <div className={classes.ContactData}>
            <h4> Enter contact data</h4>
            {formHTML}
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        loading: state.order.loading,
        token: state.authorization.idToken,
        userID: state.authorization.userID,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onOrderBurger: (orderData, token) => dispatch(orderActions.purchaseBurger(orderData, token)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withErrorhandler(ContactData, axios));
