import React, { Component, useState, useEffect } from 'react';
import Button from '../../components/UI/Button/Button';
import Input from '../../components/UI/Input/Input';
import classess from './Authorization.module.css';

import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import Spinner from '../../components/UI/Spinner/Spinner';
import { Redirect } from 'react-router';
import { updateObject, checkValidity } from '../../shared/utility';

const Authorization = (props) => {
    const [form, setForm] = useState({
        email: {
            elementType: 'input',
            elementConfig: {
                type: 'email',
                placeholder: 'Your email',
            },
            value: '',
            validation: {
                required: true,
            },
            valid: false,
            touched: false,
        },
        password: {
            elementType: 'input',
            elementConfig: {
                type: 'password',
                placeholder: 'password',
            },
            value: '',
            validation: {
                required: true,
                minLength: 8,
            },
            valid: false,
            touched: false,
        },
    });
    const [isSignUp, setIsSignUp] = useState(true);

    useEffect(() => {
        if (!props.building && props.authPath !== '/') props.onSetAuthredirectPath();
    }, []);

    const inputChangedHandler = (event, inputIdentifier) => {
        let updatedLoginForm = updateObject(form, {
            [inputIdentifier]: updateObject(form[inputIdentifier], {
                value: event.target.value,
                valid: checkValidity(event.target.value, form[inputIdentifier].validation),
                touched: true,
            }),
        });

        setForm(updatedLoginForm);
    };

    const submitHandler = (event) => {
        event.preventDefault();
        props.onAuthrization(form.email.value, form.password.value, isSignUp);
    };

    const swithAuthorizationMode = () => {
        setIsSignUp(!isSignUp);
    };

    let formElementArray = [];
    for (let key in form) {
        formElementArray.push({
            id: key,
            config: form[key],
        });
    }

    let formHTML = formElementArray.map((formElement) => (
        <Input
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={!formElement.config.valid}
            shouldValidate={formElement.config.validation}
            touched={formElement.config.touched}
            changed={(event) => inputChangedHandler(event, formElement.id)}
        />
    ));

    if (props.loading) {
        formHTML = <Spinner />;
    }

    let errorMessage = '';
    if (props.error) {
        errorMessage = <p>{props.error.message}</p>;
    }

    let authRedirect = null;
    if (props.isAuthenticated) {
        authRedirect = <Redirect to={props.authPath} />;
    }

    return (
        <div className={classess.Authorization}>
            {authRedirect}
            {errorMessage}
            <form onSubmit={submitHandler}>
                {formHTML}
                <Button btnType="Success">{isSignUp ? 'REGISTER' : 'LOGIN'}</Button>
            </form>
            <Button btnType="Danger" clicked={swithAuthorizationMode}>
                SWITH TO {isSignUp ? 'SIGN IN' : 'SIGN UP'}
            </Button>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        loading: state.authorization.loading,
        error: state.authorization.error,
        isAuthenticated: state.authorization.idToken !== null,
        building: state.burgerBuilder.building,
        authPath: state.authorization.authorizationRedirect,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onAuthrization: (email, password, isSignUp) => dispatch(actions.authorization(email, password, isSignUp)),
        onSetAuthredirectPath: () => dispatch(actions.authorizationSetRedirect('/')),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Authorization);
