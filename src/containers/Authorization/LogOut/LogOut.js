import React, { Component, useEffect } from 'react';
import { connect } from 'react-redux';
import * as actionTypes from '../../../store/actions';
import { Redirect } from 'react-router';

const LogOut = (props) => {
    useEffect(() => {
        props.onLogout();
    });

    return <Redirect to="/" />;
};

const mapDispatchToProps = (dispatch) => {
    return {
        onLogout: () => dispatch(actionTypes.logOut()),
    };
};

export default connect(null, mapDispatchToProps)(LogOut);
