import React from 'react';
import classes from './Input.module.css';

const Input = (props) => {
  let htmlInputElement = null;
  let inputClases = [classes.InputElement];

  if (props.invalid && props.shouldValidate && props.touched) {
    inputClases.push(classes.Invalid);
  }

  switch (props.elementType) {
    case 'input':
      htmlInputElement = <input className={inputClases.join(' ')} {...props.elementConfig} value={props.value} onChange={props.changed} />;
      break;
    case 'textarea':
      htmlInputElement = <textarea className={inputClases.join(' ')} {...props.elementConfig} value={props.value} onChange={props.changed} />;
      break;
    case 'select':
      htmlInputElement = (
        <select className={inputClases.join(' ')} value={props.value} onChange={props.changed}>
          {props.elementConfig.options.map((optionInput) => (
            <option key={optionInput.value} value={optionInput.value}>
              {optionInput.displayValues}
            </option>
          ))}
        </select>
      );
      break;
    default:
      htmlInputElement = <input className={inputClases.join(' ')} {...props.elementConfig} value={props.value} onChange={props.changed} />;
  }

  return (
    <div className={classes.Input}>
      <label className={classes.Label}>{props.label}</label>
      {htmlInputElement}
    </div>
  );
};

export default Input;
