import React from 'react';
import classes from './Order.module.css';

const Order = (props) => {
  let ingredientsText = [];

  for (let ingredientName in props.ingredients) {
    ingredientsText.push({ name: ingredientName, amount: props.ingredients[ingredientName] });
  }
  ingredientsText = ingredientsText.map((ig) => {
    return (
      <span key={ig.name} style={{ textTransform: 'capitalize', display: 'inline-block', margin: '0 8px', border: '1px solid gray', padding: '5px' }}>
        {ig.name} ({ig.amount})
      </span>
    );
  });

  return (
    <div className={classes.Order}>
      <p>Ingredients: {ingredientsText}</p>
      <p>
        Total Price: <strong> {Number.parseFloat(props.price).toFixed(2)}</strong>
      </p>
    </div>
  );
};

export default Order;
