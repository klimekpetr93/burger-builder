import React from 'react';
//components
import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';
//styles
import classes from './CheckOutSummary.module.css';

const CheckOutSummary = (props) => {
  return (
    <div className={classes.CheckOutSummary}>
      <h1>Doufam ze Vam chutnalo</h1>
      <div style={{ width: '100%', margin: 'auto' }}>
        <Burger ingredients={props.ingredients} />
      </div>
      <Button btnType="Danger" clicked={props.onCheckoutCanceled}>
        CANCEL
      </Button>
      <Button btnType="Success" clicked={props.onCheckoutContinued}>
        CONTINUE
      </Button>
    </div>
  );
};

export default CheckOutSummary;
